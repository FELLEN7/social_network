import React from 'react';
import s from './Dialogs.module.css';
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";
import { Field, reduxForm } from 'redux-form';


const AddMessageForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div className={s.new_message}>
        <Field component='textarea' name='newMessageBody' placeholder='Text'/>
        <button>Send</button>
      </div>
    </form>
  )
}

const AddMessageFormRedux = reduxForm({ form: 'addMessageForm' })(AddMessageForm);

const Dialogs = (props) => {
  let state = props.dialogsPage;

  let dialogsElements = state.dialogs.map(d => <DialogItem name={d.name} id={d.id} />);
  let messagesElements = state.messages.map(m => <Message message={m.message} />);
  let newMessageBody = state.newMessageBody;


  let onNewMessageChange = (e) => {
    let body = e.target.value;
    props.onNewMessageChange(body);
  }

  let onSendMessageClick = () => {
    props.onSendMessageClick();
  }

  let onSubmit = (formData) => {
    console.log('ADD MESSAGE' + formData);
  }

  return (
    <div>
      <div className={s.dialogs}>
        <div className={s.dialogsItems}>
          {dialogsElements}
        </div>
        <div className={s.messages}>
          {messagesElements}
        </div>
        <AddMessageFormRedux onSubmit={onSubmit}/>
      </div>
    </div>
  )
}


export default Dialogs;