import React from 'react';
import styles from './Users.module.css';
import profile_img from '../../assets/images/profile_img.jpeg';
import { NavLink } from "react-router-dom";


const Users = (props) => {
  let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);
  let pages = [];
  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }


  return (
    <ul className={styles.users_container}>
      <div className={styles.pages_box}>
        {pages.map((e) => {
          return <span onClick={() => props.onPageChanged(e)} className={props.currentPage === e ? [styles.selectedPage, styles.pages].join(' ') : styles.pages}>{e}</span>
        })}
      </div>
      {
        props.users.map((user) => {
          return (
            <div className={styles.user_box} key={user.id}>
              <span>
                <NavLink to={'/profile/' + user.id}>
                  <div className={styles.img_box}>
                    <img className={styles.img} src={user.photos.small ? user.photos.small : profile_img} alt="" />
                  </div>
                </NavLink>
                <div>
                  {
                    user.followed
                      ? <button
                        disabled={props.followingInProgress.some(id => id == user.id)}
                        onClick={() => { props.unfollowThunkCreator(user.id) }}>
                        unfollow
                      </button>
                      : <button
                        disabled={props.followingInProgress.some(id => id == user.id)}
                        onClick={() => { props.followThunkCreator(user.id) }}>
                        follow
                        </button>
                  }
                </div>
              </span>
              <div className={styles.info_box}>
                <span>
                  <div>{user.name}</div>
                  <div className={styles.user_status}>{user.status}</div>
                </span>
                <span>
                  {/* <div>{user.location.country}</div> */}
                  {/* <div>{user.location.city}</div> */}
                </span>
              </div>
            </div>
          )
        })
      }
    </ul>
  )
}

export default Users;