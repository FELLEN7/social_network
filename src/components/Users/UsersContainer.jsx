import React from 'react';
import { connect } from 'react-redux';
import Users from './Users';
import Preloader from '../common/Preloader/Preloader'
import {
  follow,
  unfollow,
  setUsers,
  setPage,
  setTotalCount,
  setToggleIsFetching,
  setFollowingIsFetching,
  getUsersThunkCreator,
  onPageChangedThunkCreator,
  followThunkCreator,
  unfollowThunkCreator,
} from '../../redux/usersReducer';
import {compose} from 'redux';
import { withAuthRedirect } from '../hoc/withAuthRedirect';

class UsersContainer extends React.Component {
  componentDidMount() {
    this.props.getUsersThunkCreator(this.props.currentPage, this.props.pageSize);
  }

  onPageChanged = (page_number) => {
    this.props.onPageChangedThunkCreator(page_number, this.props.pageSize);
  }

  render() {
    return (
      <>
        {this.props.isFetching ? <Preloader /> : null}
        <Users totalUsersCount={this.props.totalUsersCount}
          pageSize={this.props.pageSize}
          onPageChanged={this.onPageChanged}
          currentPage={this.props.currentPage}
          users={this.props.users}
          unfollow={this.props.unfollow}
          follow={this.props.follow}
          isFetching={this.props.isFetching} 
          setFollowingIsFetching={this.props.setFollowingIsFetching}
          followingInProgress={this.props.followingInProgress}
          followThunkCreator={this.props.followThunkCreator}
          unfollowThunkCreator={this.props.unfollowThunkCreator}
          />
      </>)
  }
}


let mapStateToProps = (state) => {
  return {
    users: state.usersPage.users,
    pageSize: state.usersPage.pageSize,
    totalUsersCount: state.usersPage.totalUsersCount,
    currentPage: state.usersPage.currentPage,
    isFetching: state.usersPage.isFetching,
    followingInProgress: state.usersPage.followingInProgress
  }
}

export default compose(
  connect(mapStateToProps, {
    follow,
    unfollow,
    setUsers,
    setPage,
    setTotalCount,
    setToggleIsFetching,
    setFollowingIsFetching,
    getUsersThunkCreator,
    onPageChangedThunkCreator,
    followThunkCreator,
    unfollowThunkCreator
  }),
  withAuthRedirect
)(UsersContainer);
