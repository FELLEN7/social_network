import React from 'react';
import { connect } from 'react-redux';
import Profile from './Profile';
import {
  getUserThunkCreator,
  getUserStatusThunkCreator,
  updateMyStatusThunkCreator
} from '../../redux/profileReducer';
import { withRouter } from 'react-router-dom';
import { withAuthRedirect } from '../hoc/withAuthRedirect';
import { compose } from 'redux';

class ProfileContainer extends React.Component {

  componentDidMount() {
    let userId = this.props.match.params.userId;
    if (!userId) {
      userId = 5307
    }
    this.props.getUserThunkCreator(userId);
    this.props.getUserStatusThunkCreator(userId);
  }

  render() {
    return (
      <Profile
        {...this.props}
        profile={this.props.profile}
        status={this.props.status}
        updateStatus={this.props.updateMyStatusThunkCreator}
      />
    )
  }
}

let mapStateToProps = (state) => ({
  profile: state.profilePage.profile,
  status: state.profilePage.status
});

export default compose(
  connect(
    mapStateToProps,
    {
      getUserThunkCreator,
      getUserStatusThunkCreator,
      updateMyStatusThunkCreator
    }
  ),
  withRouter,
  withAuthRedirect
)
  (ProfileContainer);
