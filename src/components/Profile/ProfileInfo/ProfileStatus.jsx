import React from 'react';

class ProfileStatus extends React.Component {

  state = {
    editMode: false,
    status: this.props.status
  }

  editModeOn = () => {
    this.setState({ editMode: true })
  }

  editModeOff = () => { 
    this.setState({ editMode: false });
    this.props.updateStatus(this.state.status);
  }

  onStatusChange = (e) => {
    this.setState({
      status: e.currentTarget.value
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.status !== this.props.status){
      this.setState({status: this.props.state});
    }
  }

  render() {
    return (
      <div>
        {
          !this.state.editMode &&
          <div onDoubleClick={this.editModeOn}>
            {this.props.status || '-------'}
          </div>
        }
        {
          this.state.editMode &&
          <div onBlur={this.editModeOff}>
            <input onChange={this.onStatusChange} autoFocus={true} type='text' value={this.state.status} />
          </div>
        }
      </div>
    )
  }
}

export default ProfileStatus;