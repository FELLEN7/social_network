import React from 'react';
import s from './ProfileInfo.module.css';
import ProfileStatus from './ProfileStatus';
import Preloader from '../../common/Preloader/Preloader';

const ProfileInfo = (props) => {

    return (
        <div>
            {
                !props.profile
                    ? <Preloader />
                    : <div className={s.descriptionBlock}>
                        <img src={props.profile.photos.large} alt='' />
                        <ProfileStatus 
                            status={props.status}
                            updateStatus={props.updateStatus}
                        />
                    </div>
            }
        </div>
    )
}

export default ProfileInfo;