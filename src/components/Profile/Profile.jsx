import React from 'react';
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import MyPostsContainer from './MyPosts/MyPostsContainer';
import PropTypes from 'prop-types';


const Profile = (props) => {

    return (
        <div>
            <ProfileInfo 
                profile={props.profile} 
                status={props.status}
                updateStatus={props.updateStatus}
            />
            <MyPostsContainer />
        </div>
    )
}

Profile.propTypes = {
    profile: PropTypes.object,
    status: PropTypes.string,
    updateStatus: PropTypes.func
}

export default Profile;