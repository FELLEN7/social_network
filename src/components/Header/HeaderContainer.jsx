import React from 'react';
import * as axios from 'axios';
import Header from './Header';
import {connect} from 'react-redux';
import {setUserDataThunkCreator} from '../../redux/authReducer';


class HeaderContainer extends React.Component {

    componentDidMount() {
        this.props.setUserDataThunkCreator();
    }

    render() {
        return (<Header {...this.props} />)
    }
}

const mapStateToProps = (state) => {
    return { 
        login: state.auth.login,
        email: state.auth.email,
        isAuth: state.auth.isAuth
    }
}

export default connect(mapStateToProps, {setUserDataThunkCreator})(HeaderContainer);