import React from 'react';
import styles from './Header.module.css';
import { NavLink } from 'react-router-dom';
import logo from '../../assets/images/company_logo.svg';

const Header = (props) => {
    return (
    <header className={styles.header}>
        <div className={styles.img_box}>
            <img src={logo} alt='' />
        </div>
        <div className={styles.login_box}>
            {props.isAuth
                ? <div>{props.login}<br />{props.email}</div>
                : <NavLink to={'/login'}>Login</NavLink>
            }
        </div>
    </header>)
}

export default Header;