import { usersAPI } from '../api/api';

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_PAGE = 'SET_PAGE';
const SET_TOTAL_COUNT = 'SET_TOTAL_COUNT';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS';

let initialState = {
  users: [],
  pageSize: 100,
  totalUsersCount: 0,
  currentPage: 1,
  isFetching: false,
  followingInProgress: []
}

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FOLLOW: {
      return {
        ...state,
        users: state.users.map((user) => {
          if (user.id === action.userId) {
            return {
              ...user,
              followed: true
            }
          } else {
            return user;
          }
        })
      }
    }
    case UNFOLLOW: {
      return {
        ...state,
        users: state.users.map((user) => {
          if (user.id === action.userId) {
            return {
              ...user,
              followed: false
            }
          } else {
            return user;
          }
        })
      }
    }
    case SET_USERS: {
      return {
        ...state,
        users: action.users
      }
    }
    case SET_PAGE: {
      return {
        ...state,
        currentPage: action.page
      }
    }
    case SET_TOTAL_COUNT: {
      return {
        ...state,
        totalUsersCount: action.total_count
      }
    }
    case TOGGLE_IS_FETCHING: {
      return {
        ...state,
        isFetching: action.is_fetching
      }
    }
    case TOGGLE_IS_FOLLOWING_PROGRESS: {
      return {
        ...state,
        followingInProgress:
          !action.following_progress
            ? state.followingInProgress.filter(id => id != action.id)
            : [...state.followingInProgress, action.id]
      }
    }
    default:
      return state;
  }
}

export const follow = (userId) => ({ type: FOLLOW, userId });
export const unfollow = (userId) => ({ type: UNFOLLOW, userId });
export const setUsers = (users) => ({ type: SET_USERS, users });
export const setPage = (page) => ({ type: SET_PAGE, page });
export const setTotalCount = (total_count) => ({ type: SET_TOTAL_COUNT, total_count });
export const setToggleIsFetching = (is_fetching) => ({ type: TOGGLE_IS_FETCHING, is_fetching });
export const setFollowingIsFetching = (following_progress, id) => ({ type: TOGGLE_IS_FOLLOWING_PROGRESS, following_progress, id });

export const getUsersThunkCreator = (currentPage, pageSize) => {
  return (dispatch) => {
    dispatch(setToggleIsFetching(true));
    usersAPI.getUsers(currentPage, pageSize)
      .then(data => {
        dispatch(setToggleIsFetching(false));
        dispatch(setUsers(data.items));
        dispatch(setTotalCount(data.totalCount));
      });
  }
}

export const onPageChangedThunkCreator = (page_number, pageSize) => {
  return (dispatch) => {
    dispatch(setPage(page_number));
    dispatch(setToggleIsFetching(true));
    usersAPI.getUsers(page_number, pageSize)
      .then(data => {
        dispatch(setToggleIsFetching(false));
        dispatch(setUsers(data.items));
      });
  }
}

export const followThunkCreator = (id) => {
  return (dispatch) => {
    dispatch(setFollowingIsFetching(true, id));
    usersAPI.follow(id).then(data => {
      if (data.resultCode == 0) {
        dispatch(follow(id));
      }
      dispatch(setFollowingIsFetching(false, id));
    });
  }
}

export const unfollowThunkCreator = (id) => {
  return (dispatch) => {
    dispatch(setFollowingIsFetching(true, id));
    usersAPI.unfollow(id).then(data => {
      if (data.resultCode == 0) {
        dispatch(unfollow(id));
      }
      dispatch(setFollowingIsFetching(false, id));
    });
  }
}

export default usersReducer;