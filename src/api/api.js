import * as axios from 'axios'; 

let instance = axios.create({
  withCredentials: true,
  baseURL: 'https://social-network.samuraijs.com/api/1.0/',
  headers: {
    "API-KEY": "603d4812-33e1-4d03-96c9-9484301ffa5f"
  }
})

export const profileAPI = {
  getUserProfile(id) {
    return instance.get(`profile/${id}`)
      .then(response => response.data);
  },
  getStatus(id) {
    return instance.get(`profile/status/${id}`)
      .then(response => response.data);
  },
  updateStatus(status) {
    return instance.put(`profile/status`, {status: status})
      .then(response => response.data); 
  }
}

export const usersAPI = {
  getUsers(currentPage = 1, pageSize = 10) {
    return instance.get(`users?page=${currentPage}&count=${pageSize}`)
      .then(response => response.data);
  },

  unfollow(id) {
    return instance.delete(`follow/${id}`)
      .then(response => response.data);
  },
  
  follow(id) {
    return instance.post(`follow/${id}`)
      .then(response => response.data);
  }
}

export const authAPI = {
  getMe() {
    return  instance.get(`auth/me`)
      .then(responce => responce.data);
  }
}